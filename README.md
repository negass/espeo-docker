# Example note application

### Build docker containers

    docker-compose up

### Domains
Hosts must be added to /etc/hosts

    note.dev:8080
    api.note.dev:8080

### Database
To create database schema run

    php app/console doctrine:schema:update --force

To seed database with custom data run command

    php app/console doctrine:fixtures:load

### Tests

To run tests enter commands from application directory /var/www/espeo-api

    phpunit

    bin/behat --config tests/Behat/behat.yml